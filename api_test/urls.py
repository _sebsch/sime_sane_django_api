from django.urls import path, include

from api_test.api import ImageAPIView, ListImagesAPIView


urlpatterns = [
    path('images/', ListImagesAPIView.as_view(), name="images"),
    path('images/<int:image_id>/', ImageAPIView.as_view(), name="image"),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework', ))
]
