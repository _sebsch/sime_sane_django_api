from django.shortcuts import get_object_or_404
from jsonschema.exceptions import ValidationError, SchemaError
from rest_framework import views, status
from rest_framework.response import Response

from api_test.models import Image

from api_test.schema_validators import validate_post_image_url
from api_test.serializers import ImageSerializer
from api_test.tasks import create_image_from_url


class ListImagesAPIView(views.APIView):

    @staticmethod
    def get(request):
        queryset = Image.objects.all()
        serializer = ImageSerializer(queryset, many=True)
        return Response(serializer.data)

    @staticmethod
    def post(request, *args, **kwargs):
        try:
            validate_post_image_url(request.data)
        except (SchemaError, ValidationError) as err:
            return Response(
                {
                    "Status": "error",
                    "message": f"Invalid JSON {err.message}"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        create_image_from_url.delay(url=request.data["source_url"])

        return Response({"Status": "ok"})


class ImageAPIView(views.APIView):
    @staticmethod
    def get(request, image_id):
        queryset = Image.objects.all()
        image = get_object_or_404(queryset, pk=image_id)
        serializer = ImageSerializer(image)
        return Response(serializer.data)
