import functools

import jsonschema

POST_IMAGE_JSON = {
    "name": "POST image",
    "properties": {
        "source_url": {"type": "string"}
    },
    "required": ["source_url"],
    "additionalProperties": False
}


def validate_against_schema(schema: dict):
    def inner(func):
        @functools.wraps(func)
        def wrapper(data, *args, **kwargs):
            jsonschema.validate(instance=data, schema=schema)
            return func(data, *args, **kwargs)

        return wrapper

    return inner


@validate_against_schema(POST_IMAGE_JSON)
def validate_post_image_url(data):
    return data

