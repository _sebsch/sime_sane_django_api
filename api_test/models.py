from django.db import models


class Image(models.Model):
    image = models.ImageField()
    source = models.URLField()
    description = models.TextField()



