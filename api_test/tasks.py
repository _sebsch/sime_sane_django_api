import base64

from bs4 import BeautifulSoup
from celery import shared_task
from celery.utils.log import get_task_logger
from django.core.files.images import ImageFile

from api_test.models import Image
from api_test.utils import create_session

LOGGER = get_task_logger(__name__)


@shared_task()
def download_website(url):
    client = create_session()
    return client.get(url).text


@shared_task()
def extract_elements(html, url):
    soup = BeautifulSoup(html, features="lxml")

    image_tag = soup.find('img', alt=True)
    api_path, file_name = url.rsplit('/', 1)
    image_source = f"{api_path}/{image_tag['src']}"

    description = ""
    for p_tag in (p for p in soup.find_all('p')):
        if p_tag.find("b") and "Explanation" in p_tag.find("b").string:
            description = p_tag.get_text()

    return image_source, description,


@shared_task()
def download_picture(data):

    image_source, description = data

    client = create_session()
    response = client.get(image_source)
    image = base64.encodebytes(response.content).decode('utf-8')

    return image_source, description, image


@shared_task()
def store(data):
    image_source, description, image_bytes = data
    image = Image(image=image_bytes, source=image_source, description=description)
    image.save()


@shared_task(
    autoretry_for=[IOError],
    retry_kwargs={
        'max_retries': 5,
        'countdown': 10,
        'retry_backoff': True,
    },
)
def create_image_from_url(url: str):

    chain = download_website.s(url) | extract_elements.s(url) | download_picture.s() | store.s()
    chain()




